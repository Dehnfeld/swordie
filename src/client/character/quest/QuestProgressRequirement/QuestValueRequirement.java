package client.character.quest.QuestProgressRequirement;

/**
 * Created on 3/11/2018.
 */
public interface QuestValueRequirement {

    /**
     * Returns the String corresponding to a quest progress' string value.
     * @return The String corresponding to a quest progress' string value.
     */
    String getValue();
}
