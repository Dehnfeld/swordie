package constants;

/**
 * Created on 2/18/2017.
 */
public class ServerConstants {

    public static final byte LOCALE = 8;
    public static final String WZ_DIR = "D:/SwordieMS/Swordie/WZ";
    public static final String DAT_DIR = "D:/SwordieMS/Swordie/dat";
    public static final int MAX_CHARACTERS = JobConstants.LoginJob.values().length * 3;
    public static final String SCRIPT_DIR = "D:/SwordieMS/Swordie/scripts";
    public static final String RESOURCES_DIR = "D:/SwordieMS/Swordie/resources";
    public static final short VERSION = 176;
    public static final String MINOR_VERSION = "1";
    public static final int LOGIN_PORT = 8484;
    public static final short CHAT_PORT = 8483;
}
