// important here is that all starting curly bracers are attached to the name of the scope
//1011000{
//    id 101100; // dit ook
//    everything tw o ai jaidan;
//    item1{
//        id 4310057;
//        price 91827;
//    }
//}

1011100{
    item1{
        id 2000000;
        price 30;
        tabindex 2;
    }
    item2{
        tabindex 1;
        id 2000001;
        price 50;
    }
    item3{
        tabindex 1;
        id 2000002;
        price 50;
    }
    item4{
        tabindex 1;
        id 1302208;
        price 50;
    }
}